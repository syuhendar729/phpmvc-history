<?php 


class Anime_model{
	private $table = 'anime';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}
	
	public function getAllAnime()
	{
		$this->db->query('SELECT * FROM ' . $this->table);
		return $this->db->resultSet();
	}

	public function getAnimeById($id)
	{
		$this->db->query('SELECT * FROM ' . $this->table . ' WHERE id=:id');
		// $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id=' . $id); # Hati2 Bisa kena sql-injection
		$this->db->bind('id', $id);
		return $this->db->single();
	}
}

/*private $anm = [
			[
				"judul" => "Charlotte" ,
				"genre" => "Supranatural, School, Drama" ,
				"rating" => "8,1" ,
				"sinopsis" => "Seorang MC yang memiliki kekuatan super dan ia gunakan untuk kejahatan hingga ia bertemu seseorang yang mengubah hidupnya",
				"gambar" => "charlotte.jpg"
			],
			[	
				"judul" => "Angel Beats" ,
				"genre" => "Issekai, Supranatural, School, Drama" ,
				"rating" => "8,3" ,
				"sinopsis" => "Seorang MC yang sudah mati masuk ke dunia setelah kematian karna ada sesuatu yang belum diselesaikan dalam hidupnya",
				"gambar" => "angelbeats.jpg" 
			],
			[
				"judul" => "Steins Gate" ,
				"genre" => "Sci-fi, Thriller" ,
				"rating" => "9,1" ,
				"sinopsis" => "Seorang MC yang punya kemampuan Leading Steiner yaitu bisa mengingat apa yang terjadi ketika garis waktu dunia berganti",
				"gambar" => "steinsgate.jpg"
			]
		];*/
