<?php 


class Anime_model{

	private $dbh;
	private $stmt;

	public function __construct()
	{
		// Data Source Name
		$dsn = 'mysql:host=localhost;dbname=phpmvc';
		
		try {
			$this->dbh = new PDO($dsn, 'root', '');
		} catch (PSOException $e) {
			die($e->getMessage());
		}	
	}
	
	public function getAllAnime()
	{
		$this->stmt = $this->dbh->prepare('SELECT * FROM anime');
		$this->stmt->execute();
		return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	}
}

/*private $anm = [
			[
				"judul" => "Charlotte" ,
				"genre" => "Supranatural, School, Drama" ,
				"rating" => "8,1" ,
				"sinopsis" => "Seorang MC yang memiliki kekuatan super dan ia gunakan untuk kejahatan hingga ia bertemu seseorang yang mengubah hidupnya",
				"gambar" => "charlotte.jpg"
			],
			[	
				"judul" => "Angel Beats" ,
				"genre" => "Issekai, Supranatural, School, Drama" ,
				"rating" => "8,3" ,
				"sinopsis" => "Seorang MC yang sudah mati masuk ke dunia setelah kematian karna ada sesuatu yang belum diselesaikan dalam hidupnya",
				"gambar" => "angelbeats.jpg" 
			],
			[
				"judul" => "Steins Gate" ,
				"genre" => "Sci-fi, Thriller" ,
				"rating" => "9,1" ,
				"sinopsis" => "Seorang MC yang punya kemampuan Leading Steiner yaitu bisa mengingat apa yang terjadi ketika garis waktu dunia berganti",
				"gambar" => "steinsgate.jpg"
			]
		];*/
