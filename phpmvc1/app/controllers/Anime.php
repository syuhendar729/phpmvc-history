<?php 

class Anime extends Controller{
	
	public function index()
	{
		$data["judul"] = "Daftar Anime";
		$data["anm"] = $this->model('Anime_model')->getAllAnime();
		$this->view('templates/header', $data);	
		$this->view('anime/index', $data);	
		$this->view('templates/footer');	
	}
}
