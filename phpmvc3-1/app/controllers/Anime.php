<?php 

class Anime extends Controller{
	
	public function index()
	{
		$data["judul"] = "Daftar Anime";
		$data["anm"] = $this->model('Anime_model')->getAllAnime();
		$this->view('templates/header', $data);	
		$this->view('anime/index', $data);	
		$this->view('templates/footer');	
	}

	public function detail($id)
	{
		$data["judul"] = "Detail Anime";
		$data["anm"] = $this->model('Anime_model')->getAnimeById($id);
		$this->view('templates/header', $data);	
		$this->view('anime/detail', $data);	
		$this->view('templates/footer');	
	}

	public function tambah()
	{
		if ( $_POST == NULL ) {
			header('Location: ' . BASEURL . '/Anime');
			exit;
		}

		if ( $this->model('Anime_model')->tambahDataAnime($_POST) > 0 ) {
			header('Location: ' . BASEURL . '/Anime');
			exit;
		}
	}
}
