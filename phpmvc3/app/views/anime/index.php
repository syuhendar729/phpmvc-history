<div class="container mt-3">
	<div class="row">
		<div class="col-6">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#formModal">
			Tambah Daftar Anime
			</button>
			<br><br>
			<h2>Daftar Anime</h2>
      <br>
				<ul>
			<?php foreach ($data["anm"] as $anm ):?>
					<li class="list-group-item d-flex justify-content-between align-items-center"><?= $anm["judul"]; ?>
						<a href="<?= BASEURL; ?>/Anime/detail/<?= $anm["id"] ?>" class="badge badge-primary">detail</a>
					</li>
			<?php endforeach; ?>
				</ul>
		</div>
	</div>	
</div>


<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="judulModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="judulModal">Tambah Daftar Anime</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form action="<?= BASEURL; ?>/Anime/tambah" method="post">
          <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" class="form-control" id="judul" name="judul" required="">
          </div>
          <div class="form-group">
            <label for="rating">Rating</label>
            <input type="number" class="form-control" id="rating" name="rating" placeholder="1.0" step="0.01" min="0" max="10" required="" autofocus="on">
          </div>
          <div class="form-group">
            <label for="genre">Genre</label>
            <input type="text" class="form-control" id="genre" name="genre" required="">
          </div>
          <div class="form-group">
            <label for="sinposis">Sinposis</label>
            <textarea  type="text" class="form-control" rows="3" id="sinposis" name="sinopsis" required=""></textarea>
          </div>
          <!-- <div class="form-group">
            <label for="exampleFormControlSelect1">Status</label>
            <select class="form-control" id="exampleFormControlSelect1" name="status">
              <option>Completed</option>
              <option>On-Going</option>
            </select>
          </div> -->

      </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
      </form>
    </div>
  </div>
</div>